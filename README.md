This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation

Run `npm install` to install all dependencies of the project. Once it is complete, it can be run with `npm start`.

## Requirements

NPM 6.13.7 or equivalent

This app was tested with Node 13.11.0

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

Currently, this front will only work on localhost:3001, but it could be easily mapped to a domain name or similar.

### Usage

* Input a valid url in the input box and click on `shorten!` or press enter to get a shortened url (the first 62 will only consist of a single letter or digit)
* That shortened url can be used as `http://localhost:3001/<url>` to be redirected to the original url.
* Navigate to http://localhost:3001/top_visited to see a table of the most visited shortened urls and its redirections (it will start displaying data as soon as a shortened url is created, so it will be empty at the beginning).