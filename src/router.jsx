import React from 'react';
import { BrowserRouter as Router, Route, Switch, useParams } from 'react-router-dom';
import Homepage from './Homepage';
import TopVisited from './TopVisited';

const AppRoutes = () => {
    return (
    <Router>
        <Switch>
            <Route path="/top_visited" component={TopVisited} />
            <Route path="/:url" component={Redirector} />
            <Route exact path="/" component={Homepage} />
        </Switch>
    </Router>
    );
}

const Redirector = () => {
    let {url} = useParams();

    fetch(`http://localhost:3000/shortener/get/${url}`)
    .then(res => res.json())
    .then(data => {window.location.replace(data.url)}, error => console.log(error))

    return null;
}

export default AppRoutes;