import React from 'react';
import { Table, Container, Row } from 'react-bootstrap';

const renderVisitsTable = (sites) => {
    let rows = [];
    for (const site of sites) {
        rows.push(
            <tr>
                <td>{site.url}</td>
                <td>{site.shortened_url}</td>
                <td>{site.visits}</td>
            </tr>
        )
    }
    return rows;
}

class TopVisited extends React.Component {
    constructor(props) {
        super(props);
        this.state = {sites: []}
    }
    componentDidMount() {
        fetch('http://localhost:3000/shortener/top')
        .then(res => res.json())
        .then(data => this.setState({sites: data}))
    }
    render() { 
        return (
            <Container className="pt-5 mh-100 h-100">
                <Row className="justify-content-md-center text-white"><h1 style={{fontFamily: "Montserrat"}}>Top 100 visited sites</h1></Row>
                <Row className="justify-content-md-center">
                    <Table className="text-white">
                        <thead>
                            <tr>
                                <th>URL</th>
                                <th>Shortened URL</th>
                                <th>Visits</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderVisitsTable(this.state.sites)}
                        </tbody>
                    </Table>
                </Row>
            </Container>
        );
    }
}

export default TopVisited;