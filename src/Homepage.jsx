import React from 'react';
import { Container, Row, Col, Form, Button, InputGroup, Navbar, Nav, Alert } from 'react-bootstrap'

class Homepage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {shortened_url: "", url: ""}
        this.setShortenedUrl = this.setShortenedUrl.bind(this)
        this.setURL = this.setURL.bind(this)
    }
    setURL(event) {
        this.setState({url: event.target.value, shortened_url: ""})
    }
    setShortenedUrl(event) {
        const options = {
            method: 'POST',
            body: `url=${this.state.url}`,
            headers: {"Content-Type": "application/x-www-form-urlencoded"}
        };

        fetch('http://localhost:3000/shortener/create', options)
        .then(res => res.json())
        .then(data => this.setState({shortened_url: data.shortened_url}))
        event.preventDefault();
    };
    render() {
        return(
            <Container className="pt-5 mh-100 h-100">
                <Row className="justify-content-md-center text-white"><h1 style={{fontFamily: "Montserrat"}}>URL Shortener</h1></Row>
                <Row className="justify-content-md-center">
                    <Col>
                        <Form>
                            <Form.Group>
                                <InputGroup>
                                <Form.Control 
                                    type="url" 
                                    value={this.state.url} 
                                    placeholder="URL" 
                                    className="transparent-input"
                                    onChange={this.setURL}>
                                </Form.Control>
                                <InputGroup.Append>
                                    <Button 
                                        variant="light"
                                        style={{fontFamily: "Lato"}} 
                                        type="submit" 
                                        onClick={this.setShortenedUrl}>
                                            Shorten!
                                    </Button>
                                </InputGroup.Append>
                                </InputGroup>
                                {this.state.shortened_url.length > 0 && 
                                <Alert variant="secondary">The shortened url is: {this.state.shortened_url}</Alert>}
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Homepage;