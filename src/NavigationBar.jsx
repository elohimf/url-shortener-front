import React from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap'

const NavigationBar = () => {
    return (
        <Container fluid>
            <Navbar bg="dark" expand="lg">
                <Nav className="mr-auto">
                    <Nav.Link 
                        style={{fontFamily: "Lato"}} 
                        className="text-white" 
                        href="/">
                            Home
                    </Nav.Link>
                    <Nav.Link 
                        style={{fontFamily: "Lato"}} 
                        className="text-white" 
                        href="/top_visited">
                            Top 100 visited sites
                    </Nav.Link>
                </Nav>
            </Navbar>
        </Container>
    );
}

export default NavigationBar;